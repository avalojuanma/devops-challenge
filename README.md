# Craftech Challenge - Desplegar una app con Django y React en Minikube
## ℹ️  Introduccion
En este repositorio se propone una solución al desafio del curso de DevOps dictado por Craftech Academy.

Pueden encontrar la consigna en el siguiente [link](https://github.com/craftech-academy/tpfinalcraftech-1/tree/main/DesafioFinal)
## 📋 **Requisitos** 

Antes de empezar, asegúrate de tener lo siguiente: 
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) 
- [Helm](https://helm.sh/docs/intro/install/) 
- [DockerHub](https://hub.docker.com/)  cuenta y credenciales 
- [GitLab](https://gitlab.com/)  cuenta y credenciales

## 📦 Repositorio
1. Sube el frontend y backend en su respectivo repositorio de GitLab. 
2. Agrega las variables CI/CD para cada repositorio: 
- **Frontend variables** :
```
    CI_REGISTRY_USER: <dockerhub username>
    CI_REGISTRY_PASSWORD: <dockerhub password>
    CI_REGISTRY_IMAGE: <dockerhub frontend image name>
    DEV_API_URL: http://dev.example.com/api/
    PROD_API_URL http://example.com/api/
```
- **Backend variables** :
```
    CI_REGISTRY_USER: <dockerhub username>
    CI_REGISTRY_PASSWORD: <dockerhub password>
    CI_REGISTRY_IMAGE: <dockerhub backend image name>
    TEST_ALLOWED_HOSTS: "[*]"
    TEST_DB: postgres
    TEST_DB_PASSWORD: password
    TEST_DB_USER: postgres
    TEST_DEBUG_MODE: 1
    TEST_SECRET_KEY: secret
    TEST_SQL_ENGINE: django.db.backends.postgresql
```
## 🚀 Despliegue 
1. Crea un namespace gitlab-runner y ejecuta con `kubectl apply -f cluster-initial-configs/gitlab-runner` para configurar los roles y permisos. 
2. Instala gitlab-runner para cada repositorio con Helm, agregando su respectivo `runnerRegistrationToken`.
### Monitoring
1. Crea el namespace `monitoring`.
1. Instala con Helm los paquetes de kube-prometheus-stack y loki-stack dentro del namespace `monitoring`. 
2. Importa en Grafana el dashboard `helm/kube-prometheus-stack/Dashboard-Challenge.json`.
### Volumenes
Crea los volumenes necesarios con:
```console
kubectl apply -f cluster-initial-configs/persistent-volumes.yaml
```
### App
1. Crea los namespaces dev y prod. 
2. Ejecuta Helm con los respectivos paquetes de frontend y backend, modificando los valores de las imagenes y tags.
### Hosts
1. Habilitar ingress en minikube con:
```console
minikube addons enable ingress
```
2. Obtener la IP de minikube con:
```console
minikube ip
```
2. Agregar los siguientes valores al archivo /etc/hosts:
```console
<minikube ip>		grafana.mik8s.com
<minikube ip>		dev.mik8s.com
<minikube ip>		mik8s.com
```

👨‍💻 **Contributors**  
- [Juan Avalo](https://gitlab.com/avalojuanma)
